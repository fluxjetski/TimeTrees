﻿using System;
using System.IO;
using System.Globalization;

namespace TimeTrees
{
    struct Person
    {
        public int Index;
        public string Name;
        public DateTime Birth;
        public DateTime Death;
    }
    struct TimelineEvent
    {
        public DateTime Date;
        public string Event;
    }
    class Program
    {
        const int PersonIndex = 0;
        const int NameIndex = 1;
        const int BirthIndex = 2;
        const int DeathIndex = 3;
        static void Main(string[] args)
        {
            string[] timelineFile = File.ReadAllLines("timeline.csv");
            string[] peopleFile = File.ReadAllLines("people.csv");
            TimelineEvent[] TimeLine = ParseTimeLine(timelineFile);
            Person[] People = ParsePeople(peopleFile);
            DateCheck(People);

            (int years, int months, int days) = DeltaMinAndMaxDate(TimeLine);
            Console.WriteLine($"Между макс и мин датами прошло: {years} лет, {months} месяцев и {days} дней");
        }
        static DateTime ParseDate(string value)
        {
            DateTime date;
            if (!DateTime.TryParseExact(value, "yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
            {
                if (!DateTime.TryParseExact(value, "yyyy-MM", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    if (!DateTime.TryParseExact(value, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                    {
                        date = default;
                    }
                }
            }
            return date;
        }
        static TimelineEvent[] ParseTimeLine(string[] timeline)
        {
            TimelineEvent[] time = new TimelineEvent[timeline.Length];
            for (int i = 0; i < timeline.Length; i++)
            {
                var line = timeline[i];
                object[] parts = line.Split(";");
                time[i].Date = ParseDate(parts[PersonIndex].ToString());
                time[i].Event = parts[NameIndex].ToString();
            }
            return time;
        }
        static (int, int, int) DeltaMinAndMaxDate(TimelineEvent[] timeline)
        {
            (DateTime minDate, DateTime maxDate) = GetMinAndMaxDate(timeline);
            return (maxDate.Year - minDate.Year, maxDate.Month - minDate.Month, maxDate.Day - minDate.Day);
        }
        static (DateTime, DateTime) GetMinAndMaxDate(TimelineEvent[] timeline)
        {
            DateTime minDate = DateTime.MaxValue, maxDate = DateTime.MinValue;
            for (int i = 0; i < timeline.Length; i++)
            {
                DateTime date = timeline[i].Date;
                if (date < minDate) minDate = date;
                if (date > maxDate) maxDate = date;
            }

            return (minDate, maxDate);
        }
        static void DateCheck(Person[] people)
        {

            for (int i = 0; i < people.Length; i++)
            {
                DateTime date = people[i].Birth;
                DateTime datedeath = DateTime.Now;
                if (people[i].Death != default)
                {
                    datedeath = people[i].Death;
                }

                if (DateTime.IsLeapYear(date.Year) && ((datedeath.Year - date.Year <= 20)))
                {
                    Console.WriteLine($"{people[i].Name} Не более 20 лет и родился в високосный день");
                }
            }

        }
        static Person[] ParsePeople(string[] people)
        {
            Person[] person = new Person[people.Length];
            for (int i = 0; i < people.Length; i++)
            {
                var line = people[i];
                object[] parts = line.Split(";");
                person[i].Index = int.Parse(parts[PersonIndex].ToString());
                person[i].Name = parts[NameIndex].ToString();
                person[i].Birth = ParseDate(parts[BirthIndex].ToString());
                if (parts.Length == 4)
                {
                    person[i].Death = ParseDate(parts[DeathIndex].ToString());
                }
                else
                {
                    person[i].Death = default;
                }
            }
            return person;
        }
    }
}
